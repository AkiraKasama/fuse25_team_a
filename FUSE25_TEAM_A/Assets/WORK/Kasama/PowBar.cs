﻿using UnityEngine;
using System.Collections;

public class PowBar : MonoBehaviour 
{
	public void SetScale(float x)
	{
		Vector3 localScale = this.gameObject.GetComponent<RectTransform> ().localScale;
		localScale.x = x;
		this.gameObject.GetComponent<RectTransform> ().localScale = localScale;
	}
}
