﻿using UnityEngine;
using System.Collections;

public class PowButton : MonoBehaviour
{
	private float power = 0;
	public float Power {get {return power;}}

	[SerializeField]
	private float powCoefficient = 2f;

	[SerializeField]
	private float breakCoefficient = 0.2f;

	public void OnClick()
	{
		Debug.LogError ("");
		power += powCoefficient;
	}

	public void Update()
	{
		power -= power * breakCoefficient;
		if (power < 0) 
		{
			power = 0;
		}
		Debug.LogWarning ("power : " + power);
	}
}
